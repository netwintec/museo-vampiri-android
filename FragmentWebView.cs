﻿using System;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Java.Lang;

namespace MuseoVampiri
{
	public class FragmentWebView : Fragment
	{
		const string ARG_PAGE = "ARG_PAGE";
		private int mPage;

		public static FragmentWebView newInstance(int page)
		{
			var args = new Bundle();
			args.PutInt(ARG_PAGE, page);
			var fragment = new FragmentWebView();
			fragment.Arguments = args;
			return fragment;
		}

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{

			var view = inflater.Inflate(Resource.Layout.fragmentLayoutWeb, container, false);
			var web = view.FindViewById<WebView>(Resource.Id.Web);

			web.Settings.JavaScriptEnabled = true;

			web.SetWebViewClient(new WebViewClient());
			//web.Settings.SetGeolocationEnabled(true);
			//web.Settings.SetGeolocationDatabasePath(Context.FilesDir.Path);

			web.LoadUrl("http://www.museodeivampiri.com/");
			web.Settings.CacheMode = CacheModes.CacheElseNetwork;

			return view;

		}
	}
}
