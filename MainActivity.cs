﻿using System;
using Android.App;
using Android.OS;
using Android.Support.V4.View;
using Android.Support.V4.App;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Support.V4.View;
using Android.Widget;
using Android.Graphics;
using Android.Views;
using Android.Media;
using System.Collections.Generic;
using Android.Content;
using RadiusNetworks.IBeaconAndroid;
using RestSharp;
using System.Json;
using Newtonsoft.Json.Linq;
using Android.Bluetooth;

namespace MuseoVampiri
{
	[Activity(Label = "MuseoVampiri", Icon = "@mipmap/icon" , ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
	public class MainActivity : AppCompatActivity, MediaPlayer.IOnPreparedListener, ISurfaceHolderCallback, IBeaconConsumer
	{

		public ISharedPreferences prefs;

		public FragmentVideo fVideo;
		public FragmentMainPage fMain;
		public FragmentWebView fSito;

		TextView WebText, VideoTxt, AudioTxt;
		VideoView videoView;
		MediaPlayer mp;

		public RelativeLayout back;

		bool videoplaying = false;

		public Typeface tfL, tfR, tfM, tfB, tfD;

		private const string UUID = "ACFD065E-C3C0-11E3-9BBE-1A514932AC01";
		private const string beaconId = "BlueBeacon";

		IBeaconManager _iBeaconManager;
		MonitorNotifier _monitorNotifier;
		RangeNotifier _rangeNotifier;
		RadiusNetworks.IBeaconAndroid.Region _monitoringRegion;
		RadiusNetworks.IBeaconAndroid.Region _rangingRegion;

		public bool FirstScan = false;
		public bool LockFirstScan = false;
		public bool TokenGood = false;
		public bool ScanBeIn = false;
		public int StanzaFind = 0;
		public bool Download = false;
		public bool ErrorDownload = false;
		public string PushToken;

		int porta = 82, countSleep = 51, countBeIn = 0;

		public bool SpecialBeacon = false;
		public string SpecialBeaconValue = "";

		Dictionary<int, string> BeaconMusei = new Dictionary<int, string>();
		public Dictionary<int, Stanza> StanzeDictionary = new Dictionary<int, Stanza>();
		Dictionary<int, bool> StanzeFlagDictionary = new Dictionary<int, bool>();
		private static Dictionary<string, List<int>> ListBeaconBeIn = new Dictionary<string, List<int>>();
		public static List<string> BeaconNetworks = new List<string>();

		public MainActivity()
		{
			_iBeaconManager = IBeaconManager.GetInstanceForApplication(this);

			_monitorNotifier = new MonitorNotifier();
			_rangeNotifier = new RangeNotifier();

			_monitoringRegion = new RadiusNetworks.IBeaconAndroid.Region(beaconId, UUID, null, null);
			_rangingRegion = new RadiusNetworks.IBeaconAndroid.Region(beaconId, UUID, null, null);

			BeaconMusei.Add(102, "lucca");
			BeaconMusei.Add(109, "vampiri");
		}

		public static MainActivity Instance
		{
			private set;
			get;
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);

			RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

			MainActivity.Instance = this;

			prefs = Application.Context.GetSharedPreferences("MuseoVampiri", FileCreationMode.Private);

			tfL = Typeface.CreateFromAsset(Assets, "fonts/Montserrat_Light.ttf");
			tfR = Typeface.CreateFromAsset(Assets, "fonts/Montserrat_Regular.ttf");
			tfM = Typeface.CreateFromAsset(Assets, "fonts/Montserrat_Medium.ttf");
			tfB = Typeface.CreateFromAsset(Assets, "fonts/Montserrat_Bold.ttf");
			tfD = Typeface.CreateFromAsset(Assets, "fonts/daunpenh.ttf");

			back = FindViewById<RelativeLayout>(Resource.Id.back);
			back.Visibility = ViewStates.Gone;

			back.Click += delegate
			{
				fMain.Back();

				if (fMain.isSTANZA || fMain.isNOBEAC || fMain.isBEAC)
					MainActivity.Instance.back.Visibility = ViewStates.Gone;
				else
					MainActivity.Instance.back.Visibility = ViewStates.Visible;
			};

            // Find views
			fVideo = FragmentVideo.newInstance(1);
			fMain = FragmentMainPage.newInstance(2);
			fSito = FragmentWebView.newInstance(3);

            var pager = FindViewById<CustomViewPager>(Resource.Id.pager);
			var adapter = new Pager_Adapter(this, SupportFragmentManager, fVideo, fMain, fSito);

			videoView = FindViewById<VideoView>(Resource.Id.VideoView);
			videoView.Visibility = ViewStates.Gone;

			ISurfaceHolder holder = videoView.Holder;
			holder.SetType(SurfaceType.PushBuffers);
			holder.AddCallback(this);


			var descriptor = Assets.OpenFd("video/trailer.mp4");
			mp = new MediaPlayer();
			mp.SetDataSource(descriptor.FileDescriptor, descriptor.StartOffset, descriptor.Length);
			mp.Prepare();
			mp.Completion += (sender, e) => {

				//Console.WriteLine("Finish");
				RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
				videoView.Visibility = ViewStates.Gone;

			};
			mp.Prepared += (sender, e) => { 
			
				int videoWidth = ((MediaPlayer)sender).VideoWidth;
				int videoHeight = ((MediaPlayer)sender).VideoHeight;
				float videoProportion = (float)videoWidth / (float)videoHeight;
				int screenWidth = WindowManager.DefaultDisplay.Width;// getWindowManager().getDefaultDisplay().getWidth();
				int screenHeight =  WindowManager.DefaultDisplay.Height;//getWindowManager().getDefaultDisplay().getHeight();
				float screenProportion = (float)screenWidth / (float)screenHeight;
				var lp = videoView.LayoutParameters;



			    if (videoProportion > screenProportion) {
			        lp.Width = screenWidth;
			        lp.Height = (int) ((float) screenWidth / videoProportion);
			    } else {
			        lp.Width = (int) (videoProportion* (float) screenHeight);
			        lp.Height = screenHeight;
			    }

				Console.WriteLine(videoWidth + "|" + videoHeight+"|"+videoProportion+"\n"+
				                  screenWidth + "|" + screenHeight+"|"+screenProportion+"\n"+
				                  lp.Width + "|" + lp.Height);

				videoView.LayoutParameters = lp;

			};


			var WebLayout = FindViewById<RelativeLayout>(Resource.Id.WebLayout);
			var VideoLayout = FindViewById<RelativeLayout>(Resource.Id.VideoLayout);
			var AudioLayout = FindViewById<RelativeLayout>(Resource.Id.AudioLayout);

			WebLayout.Click+= delegate {
				pager.SetCurrentItem(2, true);
			};

			VideoLayout.Click += delegate
			{
				pager.SetCurrentItem(0, true);
			};

			AudioLayout.Click += delegate
			{
				pager.SetCurrentItem(1, true);
			};

			WebText = FindViewById<TextView>(Resource.Id.WebTxt);
			VideoTxt = FindViewById<TextView>(Resource.Id.VideoTxt);
			AudioTxt = FindViewById<TextView>(Resource.Id.AudioTxt);

			WebText.Typeface = tfL;
			VideoTxt.Typeface = tfL;
			AudioTxt.Typeface = tfL;

			VideoTxt.Visibility = ViewStates.Gone;
			AudioTxt.Visibility = ViewStates.Visible;
			WebText.Visibility = ViewStates.Gone;

			pager.Adapter = adapter;
			pager.SetCurrentItem(1, false);

			pager.PageSelected += (sender, e) => {

				Console.WriteLine(e.Position);

				switch (e.Position) { 
				
					case 0:
						VideoTxt.Visibility = ViewStates.Visible;
						AudioTxt.Visibility = ViewStates.Gone;
						WebText.Visibility = ViewStates.Gone;

						MainActivity.Instance.back.Visibility = ViewStates.Gone;

						break;
						
					case 1:
						VideoTxt.Visibility = ViewStates.Gone;
						AudioTxt.Visibility = ViewStates.Visible;
						WebText.Visibility = ViewStates.Gone;

						if (fMain.isSTANZA || fMain.isNOBEAC || fMain.isBEAC)
							MainActivity.Instance.back.Visibility = ViewStates.Gone;
						else
							MainActivity.Instance.back.Visibility = ViewStates.Visible;
						
						break;
						
					case 2:
						VideoTxt.Visibility = ViewStates.Gone;
						AudioTxt.Visibility = ViewStates.Gone;
						WebText.Visibility = ViewStates.Visible;

						MainActivity.Instance.back.Visibility = ViewStates.Gone;

						break;
						
				
				}

			};


			Login();


			BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.DefaultAdapter;

			Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
			builder.SetTitle (Resources.GetString(Resource.String.Home_BT_Tit));
			builder.SetMessage (Resources.GetString(Resource.String.Home_BT_Descr));
			builder.SetCancelable (false);
			builder.SetPositiveButton (Resources.GetString(Resource.String.Home_BT_OK), delegate {
				
				mBluetoothAdapter.Enable();

				});
			builder.SetNegativeButton (Resources.GetString(Resource.String.Home_BT_KO), delegate {
				
				});
					
			if (mBluetoothAdapter == null) {
			    // Device does not support Bluetooth
			} else {
				if (!mBluetoothAdapter.IsEnabled) {
			        builder.Show ();
			    }
			}

			//** START BEACON **

			_iBeaconManager.Bind(this);

			_iBeaconManager.SetMonitorNotifier(_monitorNotifier);
			_iBeaconManager.SetRangeNotifier(_rangeNotifier);

			_monitorNotifier.EnterRegionComplete += EnteredRegion;
			_monitorNotifier.ExitRegionComplete += ExitedRegion;

			_rangeNotifier.DidRangeBeaconsInRegionComplete += RangingBeaconsInRegion;


		}

		public override void OnBackPressed()
		{
			if (videoplaying) {

				mp.Pause();
				mp.SeekTo(0);

				RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

				videoView.Visibility = ViewStates.Gone;
			}
			//base.OnBackPressed();
		}

		public void StartVideo() {


			RequestedOrientation = Android.Content.PM.ScreenOrientation.Landscape;

			videoView.Visibility = ViewStates.Visible;
			videoplaying = true;
			mp.Start();
		
		}

		public void SurfaceCreated(ISurfaceHolder holder)
		{
			Console.WriteLine("SurfaceCreated");
			mp.SetDisplay(holder);
		}
		public void SurfaceDestroyed(ISurfaceHolder holder)
		{
			Console.WriteLine("SurfaceDestroyed");
		}
		public void SurfaceChanged(ISurfaceHolder holder, Android.Graphics.Format format, int w, int h)
		{
			Console.WriteLine("SurfaceChanged");
		}
		public void OnPrepared(MediaPlayer player)
		{

		}


		//*** LOGIN *****

		public void Login() { 
			
			UtilityLoginManager loginManager = new UtilityLoginManager();
			List<string> result = loginManager.Login("a@a.it", "a");
			Console.WriteLine(result[0]);

			if (result[0] == "SUCCESS")
			{
				Console.WriteLine(result[1]);
				var prefs = MainActivity.Instance.prefs;
				var prefEditor = prefs.Edit();
				prefEditor.PutString("TokenN4U", result[1]);
				prefEditor.Commit();

				TokenGood = true;

			}

			if (result[0] == "ERROR")
			{

				var alert = new Android.Support.V7.App.AlertDialog.Builder(this);
				alert.SetTitle("Sei offline");
				alert.SetMessage("Sembra ci siano dei problemi di connessione riprova per collegarti online");
				alert.SetPositiveButton("Riprova", (senderAlert, args) => {
					Login();
				});
				alert.SetNegativeButton("Annulla", (senderAlert, args) => { });
				alert.Show();

			}
		
		}

		//***************

		//*** GESTIONE BEACON *****

		public bool IsPointInPolygon(List<Location> poly, Location point)
		{
			int i, j;
			bool c = false;

			Console.WriteLine("VALORE" + point.Lt + "|" + point.Lg);

			for (i = 0, j = poly.Count - 1; i < poly.Count; j = i++)
			{

				if ((((poly[i].Lt <= point.Lt) && (point.Lt < poly[j].Lt))
					|| ((poly[j].Lt <= point.Lt) && (point.Lt < poly[i].Lt)))
					&& (point.Lg < (poly[j].Lg - poly[i].Lg) * (point.Lt - poly[i].Lt)
						/ (poly[j].Lt - poly[i].Lt) + poly[i].Lg))
				{

					c = !c;
				}
			}

			Console.WriteLine("Final:" + c);
			return c;
		}

		void EnteredRegion(object sender, MonitorEventArgs e)
		{


			Console.WriteLine("ENTER:" + e.ToString());
			FirstScan = true;

			if (LockFirstScan)
			{
				RunOnUiThread(() =>
				{
					//fMain.ViewStanzeHome();

					fMain.stanzaFind = 1;
					fMain.ViewBeaconHome();
				});
				Download = true;
			}
		}

		void ExitedRegion(object sender, MonitorEventArgs e)
		{
			Console.WriteLine("EXIT:" + e.ToString());

			RunOnUiThread(() =>
			{
				
				fMain.BackToNoBeacon();
				back.Visibility = ViewStates.Gone;

			});

			ErrorDownload = false;
			Download = false;
			LockFirstScan = false;
		}

		void RangingBeaconsInRegion(object sender, RangeEventArgs e)
		{

			if (FirstScan && TokenGood)
			{
				if (!LockFirstScan)
				{
					LockFirstScan = true;
					var s2 = e.Beacons;
					int major = -1;
					foreach (IBeacon beacon in s2)
					{
						Console.WriteLine(beacon.Major + "|" + beacon.Minor);
						//if (beacon.Major == 102)
						//{
							major = beacon.Major;
						//}
						break;
					}
					Console.WriteLine(" MAJOR " + major);

					if (major == -1)
					{ 
						ErrorDownload = false;
						Download = false;
						LockFirstScan = false;
						return;

					}
					if (major == 109)
					{
						Console.WriteLine("CHIEDERE DATI PER IL MUSEO CON MAJOR " + major);

						//******** CHIAMATA SALVARE DATI E POI METTERE UNA VARIABILE BOOL A TRUE PER INIZIARE SCAN DATI
						//109 --> vampiri

						var client = new RestClient("http://api.netwintec.com:" + porta + "/");

						var requestN4U = new RestRequest("active-messages", Method.GET);
						requestN4U.AddHeader("content-type", "application/json");
						requestN4U.AddHeader("Net4U-Company", "museotortura");
						requestN4U.AddHeader("Net4U-Token", prefs.GetString("TokenN4U", ""));

						IRestResponse response = client.Execute(requestN4U);

						Console.WriteLine("Result:" + response.StatusCode + "|" + response.Content);

						if (response.StatusCode == System.Net.HttpStatusCode.OK && BeaconMusei.ContainsKey(major))
						{

							string museo = BeaconMusei[major];
							Console.WriteLine("MUSEO:" + museo);

							var lang = MainActivity.Instance.ApplicationContext.Resources.Configuration.Locale;

							StanzeDictionary.Clear();
                            StanzeFlagDictionary.Clear();

							JsonUtility ju = new JsonUtility();
							ju.SpacchettamentoJsonBeacon(museo, StanzeDictionary, StanzeFlagDictionary, response.Content, lang.Country);

							if (StanzeDictionary.Count != 0)
							{
								RunOnUiThread(() =>
								{
									fMain.stanzaFind = 1;
									fMain.ViewBeaconHome();
								});
							}
							else
							{

								ErrorDownload = true;

								Console.WriteLine("DIZIONARIO VUOTOOOOOO");
							}


							Download = true;
						}
						else
						{

							Download = false;

						}
					}else
						{

							Download = false;

						}

				}
				else if (!ErrorDownload) 
				{

					SpecialBeacon = false;

					Console.WriteLine("BeIn:" + countBeIn + "   " + countSleep);

					if (countSleep > 10)
					{
						if (e.Beacons.Count > 0)
						{
							var s = e.Beacons;
							foreach (IBeacon beacon in s)
							{

								Console.WriteLine(beacon.Major.ToString() + "|" + beacon.Rssi);

								if (beacon.Major.ToString() == "109"
									&& beacon.Rssi >= -99)
								{
									countBeIn = 4;
									SpecialBeacon = true;
									SpecialBeaconValue = "109";
								}

							}

							countBeIn++;
						}
						else
						{
							countBeIn = 0;
						}

						if (countBeIn == 5)
						{

							if (SpecialBeacon && SpecialBeaconValue == "109")
							{
								Stanza st = StanzeDictionary[1];
								bool flagSt = StanzeFlagDictionary[1];

								if (!flagSt)
								{

									Console.WriteLine("Ti trovi nel museo di " + st.museo + " nella stanza " + st.stanza);
									StanzaFind = 1;
									StanzeFlagDictionary[1] = true;


                                    RunOnUiThread(() =>
									{
							
										fMain.stanzaFind = 1;
										fMain.ViewBeaconHome();

									});
								}
							}

							countBeIn = 0;
							countSleep = 0;
						}
					}
					else
					{
						countSleep++;
					}

				}
			}
		}


		#region IBeaconConsumer impl
		public void OnIBeaconServiceConnect()
		{
			Console.WriteLine("Connesso  Al beacon");

			_iBeaconManager.StartMonitoringBeaconsInRegion(_monitoringRegion);
			_iBeaconManager.StartRangingBeaconsInRegion(_rangingRegion);
		}
		#endregion

		protected override void OnDestroy()
		{
			base.OnDestroy();

			Console.WriteLine("destroy");

			_rangeNotifier.DidRangeBeaconsInRegionComplete -= RangingBeaconsInRegion;
			_iBeaconManager.StopMonitoringBeaconsInRegion(_monitoringRegion);
			_iBeaconManager.StopRangingBeaconsInRegion(_rangingRegion);
			_iBeaconManager.UnBind(this);
		}

		//***************



	}
}

