﻿using System;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Java.Lang;

namespace MuseoVampiri
{
	public class FragmentVideo : Fragment
	{
		const string ARG_PAGE = "ARG_PAGE";
		private int mPage;

		public static FragmentVideo newInstance(int page)
		{
			var args = new Bundle();
			args.PutInt(ARG_PAGE, page);
			var fragment = new FragmentVideo();
			fragment.Arguments = args;
			return fragment;
		}

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{

			var view = inflater.Inflate(Resource.Layout.fragmentLayoutVideo, container, false);

			//var textView = view.FindViewById<TextView>(Resource.Id.txt);
			//textView.Text += "Fragment #" + mPage;

			var txtView = view.FindViewById<TextView>(Resource.Id.txt);
			txtView.Typeface = MainActivity.Instance.tfL;

			var imageview = view.FindViewById<ImageView>(Resource.Id.Img);

			imageview.Click += delegate
			{

				MainActivity.Instance.StartVideo();

			};

			return view;

		}
	}
}
