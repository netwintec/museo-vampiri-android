﻿using System;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V7.App;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Java.Lang;
using Square.Picasso;

namespace MuseoVampiri
{
	public class FragmentMainPage : Fragment
	{
		const string ARG_PAGE = "ARG_PAGE";
		private int mPage;

		public bool isNOBEAC = false;
		public bool isSTANZA = false;
		public bool isBEAC = false;
		public bool isInfoBEAC = false;

		RelativeLayout NOBeaconLayout;

		// STANZE
		RelativeLayout StanzeLayout;
		LinearLayout StanzeContent;
		public int stanzaFind;

		// BEACON
		RelativeLayout BeaconLayout;
		LinearLayout BeaconContent;

		// INFO BEACON
		RelativeLayout InfoBeaconLayout;
		TextView Descrizione, Titolo;
		ImageView ImageBeacon;
		HorizontalScrollView HSV;
		LinearLayout Gallery;
		ImageView Pause, Play, Stop;
		RelativeLayout VideoLayout;
		VideoView Video;
		public static MediaPlayer player;
		bool flagFirst = true, flagStop = false;
		string url_audio;

		public static FragmentMainPage newInstance(int page)
		{
			var args = new Bundle();
			args.PutInt(ARG_PAGE, page);
			var fragment = new FragmentMainPage();
			fragment.Arguments = args;
			return fragment;
		}

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{

			var view = inflater.Inflate(Resource.Layout.fragmentLayout2, container, false);

			NOBeaconLayout = view.FindViewById<RelativeLayout>(Resource.Id.NOBeaconLayout);
			NOBeaconLayout.Clickable = true;

			// STANZE VIEW
			StanzeLayout = view.FindViewById<RelativeLayout>(Resource.Id.StanzaLayout);
			StanzeLayout.Clickable = true;

			StanzeContent = view.FindViewById<LinearLayout>(Resource.Id.StanzeContent);

			StanzeLayout.Visibility = ViewStates.Gone;


			// BEACON VIEW
			BeaconLayout = view.FindViewById<RelativeLayout>(Resource.Id.BeaconLayout);
			BeaconLayout.Clickable = true;

			BeaconContent = view.FindViewById<LinearLayout>(Resource.Id.BeaconContent);

			BeaconLayout.Visibility = ViewStates.Gone;

			// BEACON Info VIEW
			InfoBeaconLayout = view.FindViewById<RelativeLayout>(Resource.Id.BeaconInfoLayout);
			InfoBeaconLayout.Clickable = true;

			Titolo = view.FindViewById<TextView>(Resource.Id.Titolo);
			Descrizione = view.FindViewById<TextView>(Resource.Id.Descrizione);

			ImageBeacon = view.FindViewById<ImageView>(Resource.Id.ImageInfoBeacon);
			Play = view.FindViewById<ImageView>(Resource.Id.playButton);
			Pause = view.FindViewById<ImageView>(Resource.Id.pauseButton);
			Stop = view.FindViewById<ImageView>(Resource.Id.stopButton);

			HSV = view.FindViewById<HorizontalScrollView>(Resource.Id.horizontalScrollView1);

			Gallery = view.FindViewById<LinearLayout>(Resource.Id.Gallery);

			VideoLayout = view.FindViewById<RelativeLayout>(Resource.Id.videoview);

			Video = view.FindViewById<VideoView>(Resource.Id.Video);
			Video.Visibility = ViewStates.Invisible;

			InfoBeaconLayout.Visibility = ViewStates.Gone;

			AudioManager a = (AudioManager)Activity.GetSystemService("audio");

			Play.Click += delegate
			{

				if (a.WiredHeadsetOn)
				{
					Console.WriteLine("PLUG");
					if (flagFirst)
					{
						player.Reset();
						player.SetDataSource(url_audio);
						player.Prepare();
						player.Start();
						
						Play.SetImageResource(Resource.Drawable.play_attivo);
						Pause.SetImageResource(Resource.Drawable.pause);

						flagFirst = false;
						Play.Clickable = false;
						Pause.Clickable = true;
						Stop.Clickable = true;
					}
					else
					{
						if (flagStop)
						{
							player.Reset();
							player.SetDataSource(url_audio);
							player.Prepare();
							player.Start();

							Play.SetImageResource(Resource.Drawable.play_attivo);
							Pause.SetImageResource(Resource.Drawable.pause);

							flagStop = false;
							Play.Clickable = false;
							Pause.Clickable = true;
							Stop.Clickable = true;
						}
						else
						{
							player.Start();

							Play.SetImageResource(Resource.Drawable.play_attivo);
							Pause.SetImageResource(Resource.Drawable.pause);

							Play.Clickable = false;
							Pause.Clickable = true;
							Stop.Clickable = true;
						}
					}
				}
				else
				{
					Console.WriteLine("UNPLUG");
					AlertDialog.Builder alert = new AlertDialog.Builder(Activity);
					alert.SetTitle("Informazione");
					alert.SetMessage("Perfavore usare un paio di cuffie per riprodurre l'audio");
					alert.SetPositiveButton("OK", (senderAlert, args) => { });
					alert.Show();
				}

			};

			Pause.Click += delegate
			{

				player.Pause();

				Play.SetImageResource(Resource.Drawable.play);
				Pause.SetImageResource(Resource.Drawable.pause_attivo);

				Play.Clickable = true;
				Pause.Clickable = false;
				Stop.Clickable = true;

			};

			Stop.Click += delegate
			{

				player.Stop();

				Play.SetImageResource(Resource.Drawable.play);
				Pause.SetImageResource(Resource.Drawable.pause);

				flagStop = true;
				Play.Clickable = true;
				Pause.Clickable = false;
				Stop.Clickable = false;

			};

			Pause.Clickable = false;
			Stop.Clickable = false;

			Video.Prepared += delegate
			{
				Console.WriteLine("PRONTO");
				//Console.WriteLine("C:" + VideoContainer.Width + "|" + VideoContainer.Height);
				//Placeholder.Visibility = ViewStates.Invisible;
				Video.Visibility = ViewStates.Visible;
				int width = (int)((VideoLayout.Height * 540) / 900);
				Video.Measure(width, VideoLayout.Height);
				Video.Start();
			};

			// VARIABILI POSIZIONE
			isNOBEAC = true;
			isSTANZA = false;
			isBEAC = false;
			isInfoBEAC = false;

			return view;

		}

		/*public void ViewStanzeHome()
		{

			// INIZILIZZAZIONE LAYOUT

			StanzeLayout.Visibility = ViewStates.Visible;
			StanzeContent.RemoveAllViews();

			isNOBEAC = false;
			isSTANZA = true;
			isBEAC = false;
			isInfoBEAC = false;

			// INIZIALIZZAZIONE VIEW

			LinearLayout.LayoutParams layoutParamsRel = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);


			RelativeLayout.LayoutParams layoutParamsImm = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);
			layoutParamsImm.AddRule(LayoutRules.CenterHorizontal, 0);

			RelativeLayout.LayoutParams layoutParamsSf = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);

			layoutParamsSf.AddRule(LayoutRules.AlignParentBottom, 1);

			RelativeLayout.LayoutParams layoutParamsText = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
			layoutParamsText.AddRule(LayoutRules.AlignParentBottom, 2);
			layoutParamsText.AddRule(LayoutRules.CenterHorizontal, 1);
			layoutParamsText.SetMargins(0, 0, 0, ConvertDpToPixel(10));

			var StanzeDictionary = MainActivity.Instance.StanzeDictionary;

			for (int i = 0; i < StanzeDictionary.Count; i++)
			{


				Stanza st = StanzeDictionary[i + 1];

				RelativeLayout rel = new RelativeLayout(Activity.ApplicationContext);
				rel.LayoutParameters = layoutParamsRel;

				ImageView im = new ImageView(Activity.ApplicationContext);
				im.SetAdjustViewBounds(true);
				im.LayoutParameters = layoutParamsImm;

				ImageView sf = new ImageView(Activity.ApplicationContext);
				sf.SetAdjustViewBounds(true);
				sf.SetImageResource(Resource.Drawable.sfocatura);
				sf.LayoutParameters = layoutParamsSf;

				TextView tx = new TextView(Activity.ApplicationContext);
				tx.Text = Context.Resources.GetString(Resource.String.StanzaH) + " " + st.stanza;
				tx.LayoutParameters = layoutParamsText;
				tx.Typeface = MainActivity.Instance.tfM;
				tx.TextSize = 18;
				tx.SetTextColor(Color.White);

				if (st.img_file == "Vampiri1")
				{
					Picasso.With(Activity.ApplicationContext)
					       	.Load(Resource.Drawable.stanzaLucca1)
					   		.Into(im);
				}

				int indice = i;
				im.Click += delegate
				{

					Console.WriteLine("PROVA:" + (i + 1) + "|" + (indice + 1));
					stanzaFind = indice + 1;
					ViewBeaconHome();

					System.GC.Collect();
				};

				rel.AddView(im);
				rel.AddView(sf);
				rel.AddView(tx);

				StanzeContent.AddView(rel);

			}


		}*/


		public void ViewBeaconHome()
		{

			// INIZILIZZAZIONE LAYOUT

			BeaconLayout.Visibility = ViewStates.Visible;
			BeaconContent.RemoveAllViews();

			isNOBEAC = false;
			isSTANZA = false;
			isBEAC = true;
			isInfoBEAC = false;

			MainActivity.Instance.back.Visibility = ViewStates.Gone;

			// INIZIALIZZAZIONE VIEW

			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(2000, LinearLayout.LayoutParams.WrapContent);
			layoutParams.Weight = (float)0.5;

			LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(2000, LinearLayout.LayoutParams.WrapContent);
			layoutParams2.Weight = (float)0.5;

			LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);

			LinearLayout.LayoutParams imgLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);
			imgLayout.SetMargins(ConvertDpToPixel(40), ConvertDpToPixel(10), ConvertDpToPixel(20), ConvertDpToPixel(10));

			LinearLayout.LayoutParams imgLayout2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);
			imgLayout2.SetMargins(ConvertDpToPixel(20), ConvertDpToPixel(10), ConvertDpToPixel(40), ConvertDpToPixel(10));

			var lang = Context.Resources.Configuration.Locale;

			var BeaconDictionary = MainActivity.Instance.StanzeDictionary[stanzaFind].BeaconDictionary;

			int i = 0;
			int count = BeaconDictionary.Count;
			LinearLayout LayoutContainer = null;
			LinearLayout ImageContainer = null;
			LinearLayout ImageContainer2 = null;
			if (count % 2 == 1)
				count++;
			for (i = 0; i < count; i++)
			{
				Console.WriteLine("for" + i);
				if (i % 2 == 0)
				{
					LayoutContainer = new LinearLayout(Activity.ApplicationContext);
					LayoutContainer.Orientation = Android.Widget.Orientation.Horizontal;
					LayoutContainer.LayoutParameters = layoutParams3;
					ImageContainer = new LinearLayout(Activity.ApplicationContext);
					ImageContainer.LayoutParameters = layoutParams;
					ImageContainer2 = new LinearLayout(Activity.ApplicationContext);
					ImageContainer2.LayoutParameters = layoutParams2;

					LayoutContainer.AddView(ImageContainer);
					LayoutContainer.AddView(ImageContainer2);

				}
				if (BeaconDictionary.ContainsKey(i))
				{

					BeaconInfo bi = BeaconDictionary[i];
					int indice = i;
					ImageView im = new ImageView(Activity.ApplicationContext);
					im.SetAdjustViewBounds(true);

					if (bi.tipo == "immagine")
					{
						Picasso.With(Activity.ApplicationContext)
									.Load(bi.url_thumbnail)
									   .Placeholder(Resource.Drawable.placeholder)
					   				.Into(im);
						
						im.Click += delegate
						{
							Console.WriteLine(i + "|" + indice);

							ViewBeaconHome(BeaconDictionary[indice].tipo, BeaconDictionary[indice]);

						};
					}
					if (bi.tipo == "immagini")
					{

						Picasso.With(Activity.ApplicationContext)
									.Load(bi.url_thumbnail)
									   .Placeholder(Resource.Drawable.placeholder)
					   				.Into(im);
						
						im.Click += delegate
						{
							Console.WriteLine(i + "|" + indice);

							ViewBeaconHome(BeaconDictionary[indice].tipo, BeaconDictionary[indice]);

						};
					}
					if (bi.tipo == "realta")
					{
						Picasso.With(Activity.ApplicationContext)
									.Load(Resource.Drawable.placeholder)
									.Placeholder(Resource.Drawable.placeholder)
					   				.Into(im);
					}
					if (bi.tipo == "video")
					{
						Picasso.With(Activity.ApplicationContext)
									.Load(bi.url_thumbnail)
									   .Placeholder(Resource.Drawable.placeholder)
					   				.Into(im);
						
						im.Click += delegate
						{
							Console.WriteLine(i + "|" + indice);

							/*
							Bundle args = new Bundle();
							args.PutString("indice", indice.ToString());
							Fragment fragment = new FragmentBeaconVideo();
							fragment.Arguments = args;
							FragmentManager frgManager = FragmentManager;
							frgManager.BeginTransaction()
								.Replace(Resource.Id.content_frame, fragment)
								.AddToBackStack(null)
								.Commit();
								*/
						};
					}
					if (i % 2 == 0)
					{
						im.LayoutParameters = imgLayout;
						ImageContainer.AddView(im);
					}
					else
					{
						im.LayoutParameters = imgLayout2;
						ImageContainer2.AddView(im);
					}

				}
				else
				{
					//ImageContainer2.LayoutParameters = layoutParams2;
				}

				if (i % 2 == 1)
				{
					BeaconContent.AddView(LayoutContainer);
				}
			}


		}

		public void ViewBeaconHome(string type, BeaconInfo bi)
		{

			// INIZILIZZAZIONE LAYOUT

			InfoBeaconLayout.Visibility = ViewStates.Visible;
			Gallery.RemoveAllViews();

			isNOBEAC = false;
			isSTANZA = false;
			isBEAC = false;
			isInfoBEAC = true;

			flagFirst = true;
			flagStop = false;
			url_audio = bi.urlaudio;

			player = new MediaPlayer();

			Titolo.Text = bi.titolo;
			Titolo.Typeface = MainActivity.Instance.tfM;
			Descrizione.Text = bi.descrizione;
			Descrizione.Typeface = MainActivity.Instance.tfL;


			MainActivity.Instance.back.Visibility = ViewStates.Visible;

			if (type == "immagine") {

				HSV.Visibility = ViewStates.Gone;
				ImageBeacon.Visibility = ViewStates.Visible;
				VideoLayout.Visibility = ViewStates.Gone;

				Play.Visibility = ViewStates.Visible;
				Pause.Visibility = ViewStates.Visible;
				Stop.Visibility = ViewStates.Visible;

				Picasso.With(Activity.ApplicationContext)
				       .Load(bi.urlImage)
				       .Placeholder(Resource.Drawable.placeholder)
					   .Into(ImageBeacon);
			
			}

			if (type == "immagini")
			{

				HSV.Visibility = ViewStates.Visible;
				ImageBeacon.Visibility = ViewStates.Gone;
				VideoLayout.Visibility = ViewStates.Gone;

				Play.Visibility = ViewStates.Visible;
				Pause.Visibility = ViewStates.Visible;
				Stop.Visibility = ViewStates.Visible;

				for (int i = 0; i < bi.ListUrlImage.Count; i++)
				{
				
					LinearLayout layout = new LinearLayout(Activity.ApplicationContext);
					layout.LayoutParameters = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.MatchParent);
					layout.SetGravity(GravityFlags.Center);

					ImageView imageView = new ImageView(Activity.ApplicationContext);
					LinearLayout.LayoutParams imgLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.MatchParent);
					imgLayout.SetMargins(ConvertDpToPixel(5), ConvertDpToPixel(0), ConvertDpToPixel(5), ConvertDpToPixel(0));
					imageView.LayoutParameters = imgLayout;
					imageView.SetImageResource(Resource.Drawable.placeholder);
					imageView.SetAdjustViewBounds(true);

					Picasso.With(Activity.ApplicationContext)
						   .Load(bi.ListUrlImage[i])
						   .Placeholder(Resource.Drawable.placeholder)
						   .Into(imageView);

					Gallery.AddView(imageView);
				}

			}

			if (type == "video")
			{
				HSV.Visibility = ViewStates.Visible;
				ImageBeacon.Visibility = ViewStates.Gone;
				VideoLayout.Visibility = ViewStates.Visible;

				Play.Visibility = ViewStates.Gone;
				Pause.Visibility = ViewStates.Gone;
				Stop.Visibility = ViewStates.Gone;

				Video.SetVideoURI(Android.Net.Uri.Parse(bi.videoUrl));
				Video.SetMediaController(new MediaController(Context));
				Video.RequestFocus();

			}

		}


		public void BackToNoBeacon() { 
		
			// INIZILIZZAZIONE LAYOUT
			StanzeLayout.Visibility = ViewStates.Gone;
			StanzeContent.RemoveAllViews();

			BeaconLayout.Visibility = ViewStates.Gone;
			BeaconContent.RemoveAllViews();

			InfoBeaconLayout.Visibility = ViewStates.Gone;
			Gallery.RemoveAllViews();

			isNOBEAC = true;
			isSTANZA = false;
			isBEAC = false;
			isInfoBEAC = false;

		}


		public void Back()
		{

			/*if (isBEAC)
			{
				// INIZILIZZAZIONE LAYOUT

				BeaconLayout.Visibility = ViewStates.Gone;
				BeaconContent.RemoveAllViews();

				isNOBEAC = false;
				isSTANZA = true;
				isBEAC = false;
				isInfoBEAC = false;

			}*/

			if (isInfoBEAC)
			{
				// INIZILIZZAZIONE LAYOUT

				InfoBeaconLayout.Visibility = ViewStates.Gone;
				Gallery.RemoveAllViews();

				isNOBEAC = false;
				isSTANZA = false;
				isBEAC = true;
				isInfoBEAC = false;

				if (player != null)
					player.Release();
				
			}


		}


		private int ConvertDpToPixel(float dp)
		{
			var pixel = (int)(dp * Resources.DisplayMetrics.Density);
			return pixel;
		}
	}
}

