﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using System.Net.Mail;
using System.Json;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MuseoVampiri
{
	[Activity (Label = "UtilityLoginManager")]			
	public class UtilityLoginManager  
	{
		bool flagNome=false,flagCognome=false,flagEMail=false,flagPW=false;
		int porta =82;

		public UtilityLoginManager() { }


		public List<string> Login(string Email, string pass)
		{
			Console.WriteLine(Email + "|" + pass);

			flagPW = false;
			flagEMail = false;

			if (pass == null || pass == "")
			{
				flagPW = true;
			}
			if (Email == null || Email == "")
			{
				flagEMail = true;
			}
			else
			{
				try
				{
					MailAddress m = new MailAddress(Email);
				}
				catch (FormatException)
				{
					flagEMail = true;
				}
			}

			if (flagPW == true || flagEMail == true)
			{

				List<string> content = new List<string>();
				content.Add("ERRORDATA");
				return content;

			}


			var client = new RestClient("http://api.netwintec.com:" + porta + "/");

			var requestN4U = new RestRequest("login/customer", Method.POST);
			requestN4U.AddHeader("content-type", "application/json");
			requestN4U.AddHeader("Net4U-Company", "museotortura");

			JObject oJsonObject = new JObject();

			oJsonObject.Add("email", Email);
			oJsonObject.Add("password", pass);

			requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

			IRestResponse response = client.Execute(requestN4U);

			Console.WriteLine("Result Login:" + response.Content+"|"+response.StatusCode);

			if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
			{

				List<string> content = new List<string>();
				content.Add("ERROR");
				return content;

			}

			if (response.StatusCode == System.Net.HttpStatusCode.OK)
			{

				List<string> content = new List<string>();
				content.Add("SUCCESS");

				JsonValue json = JsonValue.Parse(response.Content);
				content.Add(json["token"]);

				return content;

			}

			List<string> contentErr = new List<string>();
			contentErr.Add("ERROR");
			return contentErr;

		}
	}
}