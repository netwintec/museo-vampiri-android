﻿using System;
using System.Collections.Generic;
using System.Json;
using System.IO;


namespace MuseoVampiri
{		
	public class JsonUtility
	{
		public JsonUtility(){
			
		}

		public void SpacchettamentoJsonBeacon(string localita,Dictionary<int,Stanza> StanzeDictionary,Dictionary<int,bool> flag,string content,string country){


			JsonValue json = JsonValue.Parse (content);
			//Console.WriteLine (response.Content);
			JsonValue data = json ["messages"];

			 

			foreach (JsonValue dataItem in data) {
				Stanza st = new Stanza ();

				int z = 0;
				string museo = dataItem ["museo"];

				if (museo == localita) {

					st.stanza = dataItem ["stanza"];
					st.piano = dataItem ["piano"];
					st.img_file = dataItem ["image_url"];
					st.museo = museo;

					JsonValue data3 = dataItem ["coordinate"];

					foreach (JsonValue dataItem3 in data3) {

						float lon = dataItem3 ["lon"];
						float lat = dataItem3 ["lat"];

						st.Poligono.Add (new Location (lat, lon));
					}

					JsonValue data2 = dataItem ["elementi"];

					foreach (JsonValue dataItem2 in data2) {
						
						BeaconInfo bi = new BeaconInfo ();
						bi.stanza = st.stanza;
						bi.tipo = dataItem2 ["tipo"];
						if (bi.tipo == "immagine") {
							bi.url_thumbnail = dataItem2 ["thumbnail"];
							bi.urlImage = dataItem2 ["image"];
							if (country.CompareTo ("IT") == 0) {
								bi.titolo = dataItem2 ["titolo_it"];
								bi.descrizione = dataItem2 ["descrizione_it"];
								bi.urlaudio = dataItem2 ["it"];
							} else {
								bi.titolo = dataItem2 ["titolo_en"];
								bi.descrizione = dataItem2 ["descrizione_en"];
								bi.urlaudio = dataItem2 ["en"];
							}
						}

						if (bi.tipo == "realta") {

							bi.urlImage = dataItem2 ["image"];
							if (country.CompareTo ("IT") == 0) {
								bi.titolo = dataItem2 ["titolo_it"];
								bi.descrizione = dataItem2 ["descrizione_it"];
								bi.urlaudio = dataItem2 ["it"];
							} else {
								bi.titolo = dataItem2 ["titolo_en"];
								bi.descrizione = dataItem2 ["descrizione_en"];
								bi.urlaudio = dataItem2 ["en"];
							}
							bi.urlImage = dataItem2 ["image"];
							bi.url_thumbnail = dataItem2 ["thumbnail"];
							bi.folder3D = dataItem2 ["folder"];

						}

						if (bi.tipo == "immagini") {
							//bi.titolo = dataItem2 ["titolo"];
							bi.url_thumbnail = dataItem2 ["thumbnail"];
							JsonValue image = dataItem2 ["image"];
							List<String> imgApp = new List<string> ();
							int i = 0;
							foreach (JsonValue dataItem3 in image) {
								Console.WriteLine (i + ":" + image [i]);
								imgApp.Add (image [i]);
								i++;
							}
							bi.ListUrlImage = imgApp;
							if (country.CompareTo ("IT") == 0) {
								bi.titolo = dataItem2 ["titolo_it"];
								bi.descrizione = dataItem2 ["descrizione_it"];
								bi.urlaudio = dataItem2 ["it"];
							} else {
								bi.titolo = dataItem2 ["titolo_en"];
								bi.descrizione = dataItem2 ["descrizione_en"];
								bi.urlaudio = dataItem2 ["en"];
							}
						}

						if (bi.tipo == "video") {
							//bi.titolo = dataItem2 ["titolo"];
							bi.url_thumbnail = dataItem2 ["thumbnail"];
							bi.videoUrl = dataItem2 ["url"];
							if (country.CompareTo ("IT") == 0) {
								bi.titolo = dataItem2 ["titolo_it"];
								bi.descrizione = dataItem2 ["descrizione_it"];
							} else {
								bi.titolo = dataItem2 ["titolo_en"];
								bi.descrizione = dataItem2 ["descrizione_en"];
							}
						}
							
						Console.WriteLine (bi.stanza + "|" + bi.tipo + "|" + bi.titolo + "|" + bi.url_thumbnail + "|" + bi.urlImage + "|" + bi.descrizione + "|" + bi.urlaudio + "|" + bi.folder3D + "|");
						st.BeaconDictionary.Add (z, bi);
						z++;

					}

					StanzeDictionary.Add (st.stanza,st);
					flag.Add(st.stanza,false);
				}
			}
		}

	}
}


