﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace MuseoVampiri
{			
	public class BeaconInfo
	{
		public string tipo,titolo,url_thumbnail,urlImage,descrizione,urlaudio,folder3D,videoUrl;
		public int stanza;
		public List<string> ListUrlImage;
		public Bitmap Thumbnail;

		public BeaconInfo (){
			Thumbnail =null;
		}
	}
}

