﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MuseoVampiri
{
	[Activity(MainLauncher = true, NoHistory = true, Theme = "@style/Theme.Splash", ScreenOrientation = ScreenOrientation.Portrait)]
	public class SplashScreen : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.SplashScreenLayout);

			ThreadPool.QueueUserWorkItem(o => LoadActivity());

			// Create your application here
		}

		public void LoadActivity() {

			Thread.Sleep(2000);

			RunOnUiThread(() => { 
			
				var intent = new Intent(this, typeof(MainActivity));

				StartActivity(intent);

				Finish();
			
			});

		}

	}
}
