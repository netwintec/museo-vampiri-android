﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace MuseoVampiri
{
	public class Pager_Adapter : FragmentPagerAdapter
	{
		const int PAGE_COUNT = 3;
		private string[] tabTitles = { "Tab1", "Tab2" };
		readonly Context context;

		Fragment f1, f2, f3;

		public Pager_Adapter(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
		}

		public Pager_Adapter(Context context, FragmentManager fm, FragmentVideo F1,FragmentMainPage F2,FragmentWebView F3) : base(fm)
        {
			this.context = context;

			f1 = F1;
			f2 = F2;
			f3 = F3;
		}

		public override int Count
		{
			get { return PAGE_COUNT; }
		}

		public override Fragment GetItem(int position)
		{
			
			Console.WriteLine("POS:" + position);

			switch (position)
			{

				case 0:
					return f1;
					break;

				case 1:
					return f2;
					break;

				case 2:
					return f3;
					break;
					
				default:
					return f1;
					break;

			}

		}

		public override ICharSequence GetPageTitleFormatted(int position)
		{
			// Generate title based on item position
			return CharSequence.ArrayFromStringArray(tabTitles)[position];
		}

		/*public View GetTabView(int position)
		{
			// Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView
			var tv = (TextView)LayoutInflater.From(context).Inflate(Resource.Layout.custom_tab, null);
			tv.Text = tabTitles[position];
			return tv;
		}*/
	}
}
